package com.fdmgroup.model;

public class PlayerRepository implements IPlayerRepository{
	@Override
	public String getPlayerName(Player player) {
		return player.getName();
	}

	@Override
	public char getPlayerSymbol(Player player) {
		return player.getSymbol();
	}
}
