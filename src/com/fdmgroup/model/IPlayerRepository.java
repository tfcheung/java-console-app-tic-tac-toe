package com.fdmgroup.model;

public interface IPlayerRepository {
	String getPlayerName(Player player);
	char getPlayerSymbol(Player player);
}
