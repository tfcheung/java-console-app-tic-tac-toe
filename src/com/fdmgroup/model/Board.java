package com.fdmgroup.model;

public class Board {
	private static Board instance = null;
	
	private String[] cells;
	
	private Board(){
		cells = new String[9];
		for (int i = 0; i < 9; i++) {
			cells[i] = String.valueOf(i+1);
		}
	}
	
	public static Board getBoard() {
		if(instance == null) {
			instance = new Board();
		}
		return instance;
	}

	public String[] getCells() {
		return cells;
	}

	public void setCells(String[] cells) {
		this.cells = cells;
	}
	
	public String toString() {
		return " ---|---|--- " + '\n' +
				"| " + cells[0] + " | " + cells[1] + " | " + cells[2] + " |" + '\n' +
				"|-----------|" + '\n' +
				"| " + cells[3] + " | " + cells[4] + " | " + cells[5] + " |" + '\n' +
				"|-----------|" + '\n' +
				"| " + cells[6] + " | " + cells[7] + " | " + cells[8] + " |" + '\n' +
				" ---|---|--- ";
	}
}
