package com.fdmgroup.view;

import com.fdmgroup.controller.GameController;
import com.fdmgroup.controller.GameManager;
import com.fdmgroup.exception.TiedException;
import com.fdmgroup.exception.WinnerException;
import com.fdmgroup.model.Player;

public class GameRunView {
	public static void gameStart(Player player1, Player player2) throws WinnerException, TiedException {
		try {
			GameController gc = new GameController();
			GameManager gm = new GameManager();

			System.out.println("Please enter number 1 ~ 9 to mark your symbol into the cell");
			gc.printBoard();
			gc.gameProcess(player1, player2);
			endGameMessage(gm.checkWinner());
		}
		catch(WinnerException e) {
			GameManager gm = new GameManager();
			if(gm.checkWinner() == "player1") {
				System.out.print("The winner is " + player1.getName());
			}
			else System.out.println("The winner is " + player2.getName());
		}
		catch(TiedException t) {
			System.out.println("The game is tied");
		}
	}
	
	public static void endGameMessage(String egMessage) throws WinnerException, TiedException{
		if(!egMessage.equalsIgnoreCase("tied")){
			throw new WinnerException(); 
		}
		else throw new TiedException();
	}
	
	public static void playerTurnMessage(Player player) {
		System.out.println(player.getName() + 
				" , this is you turn. Please Enter a slot number (1~9) to place your symbol " +
				player.getSymbol() + " in: ");
	}
}
