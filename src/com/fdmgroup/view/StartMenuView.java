package com.fdmgroup.view;

import java.util.Scanner;

import com.fdmgroup.exception.TiedException;
import com.fdmgroup.exception.WinnerException;
import com.fdmgroup.model.Player;

public class StartMenuView {
	static Player player1;
	static Player player2;
	private static Scanner scanner = new Scanner(System.in);
	public static void setup() throws WinnerException, TiedException {
		System.out.println("Welcome to Tic-Tac-Toe");
		System.out.println("------------------");
		System.out.println("Player1 Please Enter Your Full Name");
		player1 = new Player(scanner.next(), 'O');
		System.out.println(player1.getName() + ", your symbol is " + "'" + player1.getSymbol() + "'" );
		System.out.println("------------------");
		System.out.println("Player2 Please Enter Your Full Name");
		player2 = new Player(scanner.next(), 'X');
		System.out.println(player2.getName() + ", your symbol is " + "'" + player2.getSymbol() + "'" );
		System.out.println("------------------");
		GameRunView.gameStart(player1,player2);
	}
}
