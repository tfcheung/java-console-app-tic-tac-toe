package com.fdmgroup.controller;

import java.util.Arrays;

import com.fdmgroup.model.Board;

public class GameManager {
	Board board;
	public GameManager(){
		board = Board.getBoard();
	}
	
	public Boolean validInput(String input) {
		Boolean result = false;
		if(Integer.parseInt(input) <= 9 && Integer.parseInt(input) >= 1) {
			if(Character.isDigit(board.getCells()[Integer.parseInt(input) - 1].charAt(0))) {
				return true;
			}
			else {
				System.out.println("Invalid input, the spot has been taken");
			}
		}
		else {
			System.out.println("Invalid input, the input has to be between 1~9");
		}
		return result;
	}
	
	public String checkWinner() {
		String[] cells = board.getCells();
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = "" + cells[0] + cells[1] + cells[2];
				break;
			case 1:
				line = "" + cells[3] + cells[4] + cells[5];
				break;
			case 2:
				line = "" + cells[6] + cells[7] + cells[8];
				break;
			case 3:
				line = "" + cells[0] + cells[3] + cells[6];
				break;
			case 4:
				line = "" + cells[1] + cells[4] + cells[7];
				break;
			case 5:
				line = "" + cells[2] + cells[5] + cells[8];
				break;
			case 6:
				line = "" + cells[0] + cells[4] + cells[8];
				break;
			case 7:
				line = "" + cells[2] + cells[4] + cells[6];
				break;
			}
			if (line.equals("OOO")) {
				return "player1";
			} else if (line.equals("XXX")) {
				return "player2";
			}
		}

		for (int a = 1; a <= 9; a++) {
			if (Arrays.asList(cells).contains(String.valueOf(a))) {
				break;
			}
			else if (a == 8) {
				return "tied";
			}
		}
		return null;
	}
}
