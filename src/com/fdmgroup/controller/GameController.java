package com.fdmgroup.controller;

import java.util.Scanner;

import com.fdmgroup.model.Board;
import com.fdmgroup.model.Player;
import com.fdmgroup.view.GameRunView;

public class GameController {
	private Board board;
	private GameManager gm = new GameManager();
	
	public GameController() {
		super();
		this.board = Board.getBoard();
	}
	
	public void printBoard() {
		System.out.println(board.toString());
	}
	
	public void clearBoard() {
		for (int i = 0; i < 9; i++) {
			board.getCells()[i] = String.valueOf(i+1);
			System.out.println(board.toString());
		}
	}
	
	public void gameProcess(Player player1, Player player2) {
		Scanner scan = new Scanner(System.in);

		while(gm.checkWinner() == null) {
			String input;
			Boolean checkInput;
			Player[] players = {player1, player2};
			
			for(int i = 0; i < players.length; i++) {
				GameRunView.playerTurnMessage(players[i]);
				do {
					input = scan.next();
					checkInput = gm.validInput(input);
					if(checkInput) {
						board.getCells()[Integer.parseInt(input) - 1] = "" + players[i].getSymbol();
						printBoard();
					}
				}
				while(checkInput != true);
				
				if(gm.checkWinner() != null) {
					break;
				}
			}
		}
		scan.close();
	}
}
